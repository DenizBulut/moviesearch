import React, { Component } from 'react';
import './App.css';
import { Button } from 'react-bootstrap';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Form, FormControl } from 'react-bootstrap';
import './bootstrap.css'
import MovieCard from './movieCard';
import { Row, Col, Container } from 'react-bootstrap';

export default class App extends Component {
  

  constructor(props) {
    super(props);
    this.state = {
      movie:"",
      movieTemp:"",
      key:1
    };

  }
  

  render()
  {
      return (
        <div style={{width:"100%"}}>
          <Row >
          <Col>
           <Navbar bg="dark" variant="dark">
              <Navbar.Brand href="#home">Movie Searcher</Navbar.Brand>
              <Form inline>
              <FormControl 
                type="text" 
                placeholder="Search" 
                className="mr-sm-2" 
                defaultValue={this.state.movie}
                onChange={(e)=> this.setState({movie:e.target.value})}/>
              <Button variant="outline-info" onClick={()=> this.setState({movieTemp:this.state.movie , key:Math.random()})}>Search</Button>
             </Form>
            </Navbar>
            </Col>
          </Row>
          <br></br>
          
          <div style={{width:"100%" , marginLeft:"auto" , marginRight:"auto"}}>
       
          {
            this.state.movieTemp!=""?
            <MovieCard keyword={this.state.movieTemp} key={this.state.key}></MovieCard>:""

          }
       </div>
       
        
     
        
        </div>
      );
    

  }
 
}