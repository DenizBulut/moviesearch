import React, { Component } from 'react';
import './App.css';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import './bootstrap.css'
import { Row, Col, Container } from 'react-bootstrap';
import Pagination from 'react-bootstrap/Pagination'
import {Modal} from 'react-bootstrap';

export default class MovieCard extends Component {
  
    constructor(props) {
        super(props);
        this.state = {
          data:[],
          show:false,
          selectedMovie:null,
          activePage:1
        };
        this.handleChange=this.handleChange.bind(this)
      }

      componentDidMount() {
        fetch(' http://www.omdbapi.com/?s='+this.props.keyword+'&apiKey=ba1e0e2a&page=1')
          .then(response => response.json())
          .then(data => this.setState({ data }));
      }
      handleChange(e)
      {
          this.setState({selectedMovie:e , show:true})
      }
    
      render()
      {
        let i=0;
        
        var movieList=[]
        let items = [];
        if(this.state.data.Search)
        {
          for(let i=1;i<this.state.data.Search.length%8+1;i++) //her sayfada max 8 film listelenmesi için gereken page sayısı hesaplanıyor.
          {
              items.push(
                <Pagination.Item key={i} active={i === 0}>
                {i}
                </Pagination.Item>,
              );

          }
          
          var movieList2=[]

          return (
          <div>
            {
            this.state.selectedMovie?

              <Modal show={this.state.show} onHide={()=>this.setState({show:false})}>
                <Modal.Header closeButton>
                  <Modal.Title>{this.state.selectedMovie.Title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {"YEAR: " +this.state.selectedMovie.Year}
                  <br></br>
                  
                  </Modal.Body>
                <Modal.Footer> 
                  <Button variant="outline-warning" style={{marginLeft:"5px"}} onClick={()=>  window.location.href =`https://www.imdb.com/title/` +this.state.selectedMovie.imdbID}>Go IMDB</Button>
                  <Button variant="secondary" onClick={()=>this.setState({show:false, key:Math.random()})}>
                  Close
                  </Button>
                </Modal.Footer>
              </Modal>:""
              }
            
             <Row>
                
                {
                  
                  this.state.data.Search.map(key => 
                    {
                      
                    if(i>=(parseInt(this.state.activePage)*8)-8 && i<parseInt(this.state.activePage)*8) //her pagede 8 film listeleniyor.
                    {
                      
                      if(this.state.data.Search[i])
                      {
                        movieList2.push(
                        
                          <Col xs={12} md={4}
                           xl={3} sm={6}>
                            <Card style={{ width: '256px' , backgroundColor:"#343a40", height:'480px'}}>
                            <Card.Img variant="top" src={key.Poster} width='256px' height='350px'/>
                            <Card.Body>
                              <Card.Title style={{color:"white", fontSize:"15px"}}>{key.Title}</Card.Title>
    
                              <Button variant="outline-info" onClick={()=> this.setState({show:true , selectedMovie:key})}>Details</Button>
                            </Card.Body>
                            </Card>
                            <br></br>
                          </Col>
                          )
                      }
                    }
                    i++;
                  })
                }
                {movieList2}
                
              </Row>
              <Row style={{marginLeft:"10px"}}>
                <Pagination
                onClick={(e)=>this.setState({activePage:e.target.text})}
                >{items}</Pagination>
              </Row>  
            </div>
          );
        }

          else
            return(
              ""
            )
      

        
        
       
      }
     
    
}
